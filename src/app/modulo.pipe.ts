import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'modulo'
})
export class ModuloPipe implements PipeTransform {

  transform(value: number, number: number): number {
    return value % number;
  }

}
