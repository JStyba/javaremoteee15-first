import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';

@Component({
  selector: 'sda-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  @ViewChild('f', {static: true}) form: FormGroup;
  constructor() { }

  ngOnInit(): void {
    console.log(this.form);
  }

  onSubmit(form: NgForm): void {
    console.log(form.value);
  }

}
