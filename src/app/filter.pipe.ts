import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(array: string[], startsWith: string): string[] {
    let temp: string[] = [];
    temp = array.filter(item => item.startsWith(startsWith));
    return temp;
  }

}
